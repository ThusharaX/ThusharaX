﻿<h1 align="center">
  <img src="https://raw.githubusercontent.com/ThusharaX/ThusharaX/master/name.svg" alt="Marton Lederer" />
</h1>
<h3 align="center">Student and Developer</h3>

</br>

![](https://www.codewars.com/users/ThusharaX/badges/large)

<a href="https://dev.to/thusharax">
  <img src="https://d2fltix0v2e0sb.cloudfront.net/dev-badge.svg" alt="Thushara Thiwanka's DEV Community Profile" height="30" width="30">
</a>

[![Twitter Follow](https://img.shields.io/twitter/follow/Thushara_X?color=1DA1F2&logo=twitter&style=for-the-badge)](https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fgithub.com%2FThushara_X&screen_name=Thushara_X)

![](https://komarev.com/ghpvc/?username=ThusharaX&style=flat&color=brightgreen)


<hr style="border:0px solid gray; height:1.5px"> </hr>

- 🌱 I’m currently learning everything 🤣
- 👯 I’m looking to collaborate with other content creators
- 🥅 2021 Goals: Contribute more to Open Source projects

### Connect with me:

[<img align="left" alt="TTiwanka | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />][twitter]
[<img align="left" alt="thushara-thiwanka-0b95b614a | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
[<img align="left" alt="thushara_thiwanka | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />][instagram]


</br>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=thusharax&show_icons=true&locale=en" alt="thusharax" /></p>

<p><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs?username=thusharax&show_icons=true&locale=en&layout=compact" alt="thusharax" /></p>



[twitter]: https://twitter.com/TTiwanka
[instagram]: https://instagram.com/thushara_thiwanka
[linkedin]: https://linkedin.com/in/thushara-thiwanka-0b95b614a
